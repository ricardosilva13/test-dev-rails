# Test Dev Rails

Criar uma rede social (basica), com:

  - Sign in & Sign up usando devise
  - Perfil de usuário com informações e foto
  - Solicitação de amizade, com opção de aceitar ou recusar a solicitação
  - Lista de amigos 
  - Envio de mensagens para amigos
  - Usar bootstrap para estilizar o layout

#### Como deve ser feito o teste

> Cada candidato terá um branch com seu nome e deverá submeter
> sua solução para o mesmo.


O candidato terá até terça dia 8 de setembro ás 23:59h para entregar a solução.


###### Boa sorte !!!
